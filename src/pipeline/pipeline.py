import os
from azureml.core import Workspace, Experiment
from azureml.core.environment import Environment
from azureml.core.compute import AmlCompute
from azureml.core.compute import ComputeTarget
from azureml.core.runconfig import RunConfiguration
from azureml.core.authentication import InteractiveLoginAuthentication
from azureml.pipeline.core import Pipeline, PipelineData
from azureml.pipeline.steps import PythonScriptStep
from azureml.core.model import Model, InferenceConfig
from azureml.core.webservice import AciWebservice, Webservice

# Load the serive config file
with open("service-config.json", "r") as f:
    service_config = json.load(f)

# Load the model config file
with open("service-config.json", "r") as f:
    model_config = json.load(f)

# Access the values in the config files
workspace_name = service_config["workspace_name"]
subscription_id = service_config["subscription_id"]
resource_group = service_config["resource_group"]
service_name = service_config["service_name"]
env_name = service_config['env_name']
model_name = model_config['model_name']

# Define the workspace and experiment
ws = Workspace.get(name=workspace_name,
               subscription_id=subscription_id,
               resource_group=resource_group)
experiment_name = 'iris-mlflow-pipeline'
exp = Experiment(workspace=ws, name=experiment_name)

# Create or attach an existing compute target
compute_name = "cpu-cluster"
if compute_name not in ws.compute_targets:
    compute_config = AmlCompute.provisioning_configuration(vm_size="STANDARD_DS3_V2", max_nodes=4)
    compute_target = ComputeTarget.create(ws, compute_name, compute_config)
    compute_target.wait_for_completion(show_output=True)
else:
    compute_target = ws.compute_targets[compute_name]

# Create a PipelineData object to share data between pipeline steps
data_folder = PipelineData(name="data_folder", datastore=ws.get_default_datastore())

# Define the environment to run the pipeline steps
myenv == Environment.get(workspace=ws, name=env_name)


# Configure the run configuration for the pipeline steps
run_config = RunConfiguration()
run_config.environment = myenv
run_config.target = compute_target

# Define the pipeline steps as PythonScriptStep objects
load_data_step = PythonScriptStep(
    name="load_data",
    script_name="load_data.py",
    arguments=[
        "--output_folder", data_folder
    ],
    inputs=[],
    outputs=[data_folder],
    compute_target=compute_target,
    runconfig=run_config,
    source_directory="./scripts"
)

train_model_step = PythonScriptStep(
    name="train_model",
    script_name="train_model.py",
    arguments=[
        "--data_folder", data_folder,
        "--model_name", model_name
    ],
    inputs=[data_folder],
    compute_target=compute_target,
    runconfig=run_config,
    source_directory="./scripts"
)
"--model_name", type=str, help="Name of the registered model")

register_model_step = PythonScriptStep(
    name="register_model",
    script_name="register_model.py",
    arguments=[
        "--model_name", model_name,
        "--subscription_id", subscription_id,
        "--resource_group", resource_group,
        "--workspace_name", workspace_name,
        "--databricks_token",""
    ],
    inputs=[],
    compute_target=compute_target,
    runconfig=run_config,
    source_directory="./scripts"
)

deploy_model_step = PythonScriptStep(
    name="deploy_model",
    script_name="deploy_model.py",
    arguments=[
        "--model_name", model_name,
        "--deployment_name", service_name,
        "--subscription_id", subscription_id,
        "--resource_group", resource_group,
        "--workspace_name", workspace_name,
        "--model_version", ""
    ],
    inputs=[],
    compute_target=compute_target,
    runconfig=run_config,
    source_directory="./scripts"
)

# Define the pipeline as a list of steps
pipeline = Pipeline(workspace=ws, steps=[load_data_step, train_model_step, register_model_step, deploy_model_step])

# Submit the pipeline run
pipeline_run = exp.submit(pipeline)

# Wait for the pipeline run to complete
pipeline_run.wait_for_completion(show_output=True)

#
