import mlflow
import mlflow.sklearn
import pandas as pd
import matplotlib.pyplot as plt

from mlflow.tracking.client import MlflowClient
from mlflow.entities import ViewType
from mlflow.store.artifact.models_artifact_repo import ModelsArtifactRepository

from numpy import savetxt

from azureml.core.webservice import AciWebservice, Webservice
from azureml.core.model import Model

def save_model(clf, accuracy, workspace):
    # Load the config file
    with open("model-config.json", "r") as f:
        config = json.load(f)

    # Access the values in the config file
    model_name = config["model_name"]
    model_stage = config["model_stage"]
    model_description = config["model_description"]

    # Register the model in mlflow
    with mlflow.start_run():
        # Log the hyperparameters and metric values
        hyperparams = {
            "criterion": clf.criterion,
            "max_depth": clf.max_depth,
            "min_samples_split": clf.min_samples_split
        }
        for name, value in hyperparams.items():
            mlflow.log_param(name, value)

        mlflow.log_metric("accuracy", accuracy)

        # Log the model as an artifact
        mlflow.sklearn.log_model(clf, "model")

        # Retrieve the best run
        nb_path = dbutils.widgets.get("Current_Notebook_Path")
        experiment_id = mlflow.get_experiment_by_name(nb_path).experiment_id
        client = MlflowClient()
        best_run = client.search_runs(
            experiment_ids=[experiment_id],
            run_view_type=mlflow.entities.ViewType.ACTIVE_ONLY,
            max_results=1,
            order_by=["metrics.accuracy DESC"]
        )[0]
        run_id = best_run.info.run_id

        # Register the model in Azure ML
        model_path = f"models:/{model_name}/{model_stage}"
        os.makedirs("model", exist_ok=True)
        local_path = ModelsArtifactRepository(model_path).download_artifacts("",dst_path="model")
        #Registiring Model in MLFlow
        model_uri = f"runs:/{run_id}/model"
        model = mlflow.register_model(model_uri, model_name)

        # Transition the model version to the staging stage
        client.transition_model_version_stage(
          name=model.name,
          version=model.version,
          stage=model_stage,
        )

        model = Model.register(
            workspace=workspace,
            model_path=local_path,
            model_name=model_name,
            description=model_description
        )
        return model
