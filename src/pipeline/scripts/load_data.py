import os
import pandas as pd
from sklearn.datasets import load_iris
from azureml.core import Dataset, Run

def save_data(data, output_folder):
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)
    df = pd.DataFrame(data.data, columns=data.feature_names)
    df["target"] = data.target
    output_path = os.path.join(output_folder, "iris.csv")
    df.to_csv(output_path, index=False)
    print(f"Data saved to {output_path}")

if __name__ == "__main__":
    run = Run.get_context()
    iris_data = load_iris()

    save_data(iris_data, run.input_datasets["data_folder"])
