import argparse
from azureml.core import Workspace
from azureml.core.webservice import AciWebservice, Webservice
from azureml.core.model import Model
from azureml.exceptions import WebserviceException
import mlflow.azureml

def deploy_model(model_name, deployment_name, subscription_id, resource_group, workspace_name, model_version=None):
    ws = Workspace(subscription_id=subscription_id, resource_group=resource_group, workspace_name=workspace_name)
    model = None
    if model_version is not None:
        model = Model(ws, name=model_name, version=model_version)
    else:
        model = Model(ws, name=model_name)
    try:
        webservice = Webservice(ws, name=deployment_name)
        print(f"{deployment_name} already exists, updating...")
        webservice.update(models=[model])
    except WebserviceException:
        print(f"{deployment_name} does not exist, creating...")
        aci_config = AciWebservice.deploy_configuration(cpu_cores=1, memory_gb=1)
        mlflow_model = mlflow.azureml.load_model(model_name, workspace=ws)
        service = Model.deploy(ws, deployment_name, [model], inference_config=mlflow_model.inference_config, deployment_config=aci_config)
        service.wait_for_deployment(show_output=True)
    endpoint = service.scoring_uri
    print(f"{deployment_name} deployed to ACI webservice at {endpoint}")

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--model_name", type=str, help="Name of the registered model")
    parser.add_argument("--deployment_name", type=str, help="Name of the deployment")
    parser.add_argument("--subscription_id", type=str, help="Azure subscription ID")
    parser.add_argument("--resource_group", type=str, help="Azure resource group name")
    parser.add_argument("--workspace_name", type=str, help="Azure Machine Learning workspace name")
    parser.add_argument("--model_version", type=int, help="Version of the registered model")
    args = parser.parse_args()
    deploy_model(args.model_name, args.deployment_name, args.subscription_id, args.resource_group, args.workspace_name, args.model_version)
