import os
import mlflow
import mlflow.azureml
import argparse
from azureml.core import Workspace
from azureml.core.model import Model
from mlflow.store.artifact.models_artifact_repo import ModelsArtifactRepository

def register_model(model_name, subscription_id, resource_group, workspace_name, databricks_token):
    mlflow.set_experiment(model_name)
    model_path = f"models:/{model_name}/{model_stage}"
    os.makedirs("model", exist_ok=True)
    local_path = ModelsArtifactRepository(model_path).download_artifacts("",dst_path="model")
    with mlflow.start_run():
        mlflow.sklearn.load_model(local_path)
        run_id = mlflow.active_run().info.run_id
        model_uri = f"runs:/{run_id}/model"
        ws = Workspace(subscription_id=subscription_id, resource_group=resource_group, workspace_name=workspace_name)
        model = Model.register(workspace=ws, model_path=model_uri, model_name=model_name, tags=mlflow.get_tags())
        print(f"Model {model.name} registered in workspace {ws.name}")
        mlflow.azureml.log_model(ws, model.name, registered_model_name=model_name, description=model.description, workspace_token=databricks_token)
        print(f"Model {model.name} logged in Azure ML")

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--model_name", type=str, help="Name of the registered model")
    parser.add_argument("--subscription_id", type=str, help="Azure subscription ID")
    parser.add_argument("--resource_group", type=str, help="Azure resource group name")
    parser.add_argument("--workspace_name", type=str, help="Azure Machine Learning workspace name")
    parser.add_argument("--databricks_token", type=str, help="Databricks workspace token")
    args = parser.parse_args()
    register_model( args.model_name, args.subscription_id, args.resource_group, args.workspace_name, args.databricks_uri, args.databricks_token)
