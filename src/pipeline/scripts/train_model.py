import os
import mlflow
import argparse
import pandas as pd
from sklearn.datasets import load_iris
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from data_transformation import encode_class_labels, scale_features

def train_model(data_path, model_path, C):
    iris_df = pd.read_csv(data_path)
    X = iris_df.iloc[:, :-1]
    y = iris_df.iloc[:, -1]
    y = encode_class_labels(y)
    X = scale_features(X)
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)
    clf = LogisticRegression(C=C)
    clf.fit(X_train, y_train)
    y_pred = clf.predict(X_test)
    accuracy = accuracy_score(y_test, y_pred)
    print(f"Accuracy: {accuracy}")
    
    os.makedirs(model_path, exist_ok=True)
    model_file_path = os.path.join(model_path, "model.pkl")
    with open(model_file_path, "wb") as f:
        pickle.dump(clf, f)
    mlflow.log_param("C", C)
    mlflow.log_metric("accuracy", accuracy)
    mlflow.sklearn.log_model(clf, "model")

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--data_path", type=str, help="Path to input data")
    parser.add_argument("--model_path", type=str, help="Path to output model")
    parser.add_argument("--C", type=float, default=1.0, help="Inverse of regularization strength")
    args = parser.parse_args()
    with mlflow.start_run():
        train_model(args.data_path, args.model_path, args.C)
