from register_model import save_model
from training.trainer.task import train_model

#Model Training Pipeline
clf, accuracy = train_model()

model = save_model(clf, accuracy)

# Save the model to disk
joblib.dump(model, 'model')
