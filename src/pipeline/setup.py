from setuptools import setup, find_packages

setup(
    name='iris-pipeline',
    version='0.1.0',
    description='Iris pipeline module',
    author='Salman Shaik',
    author_email='salmanshaikdev@gmail.com',
    packages=find_packages(),
    install_requires=[
        'mlflow',
        'azureml-sdk',
        'azureml-core'
    ],
)
