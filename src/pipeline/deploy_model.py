import os
import json
import azureml
import azureml.core
from azureml.core import Workspace, Experiment
from azureml.core.compute import ComputeTarget, DatabricksCompute
from azureml.core.environment import Environment,CondaDependencies
from azureml.core.model import InferenceConfig, Model
from azureml.core.webservice import AciWebservice, Webservice
from azureml.exceptions import ComputeTargetException

#Uncomment to Create workspace

# subscription_id = "0f101778-d255-42aa-817d-531d4181f626" #you should be owner or contributor
# resource_group = "iris-databricks" #you should be owner or contributor
# workspace_name = "iris-databricks-ws" #your workspace name
# workspace_region = "southeastasia" #your region (if workspace need to be created)
#
# workspace = Workspace.create(name = workspace_name,
#                              location = workspace_region,
#                              resource_group = resource_group,
#                              subscription_id = subscription_id,
#                              exist_ok=True)


# Load the config file
with open("service-config.json", "r") as f:
    config = json.load(f)

# Access the values in the config file
workspace_name = config["workspace_name"]
subscription_id = config["subscription_id"]
resource_group = config["resource_group"]
env_name = config['env_name']
conda_dep_pkgs = config['conda_dep_pkgs']
pip_pkgs = config['pip_pkgs']
entry_script = config['entry_script']
prod_webservice_name = config['prod_webservice_name']
service_name = config["service_name"]
cpu_cores = config["cpu_cores"]
memory_gb = config["memory_gb"]
auth_enabled = config["auth_enabled"]

#Fetching Workspace
ws = Workspace.get(name=workspace_name,
               subscription_id=subscription_id,
               resource_group=resource_group)

# Choosing AzureML-Minimal and customizing as required ( see the last cell to display available environments)
env = Environment.get(workspace=ws, name=env_name)
curated_clone = env.clone("customize_curated")

conda_dep = CondaDependencies()

# Install additional packages as required
for conda_dep_pkg in conda_dep_pkgs:
  conda_dep.add_conda_package(conda_package=conda_dep_pkg)

for pip_pkg in pip_pkgs:
  conda_dep.add_pip_package(pip_package=pip_pkg)

curated_clone.python.conda_dependencies=conda_dep

prod_webservice_deployment_config = AksWebservice.deploy_configuration()

inference_config = InferenceConfig(entry_script=entry_script, environment=curated_clone)

# Deploy the model to Azure Container Instance (ACI)
deploy_config = AciWebservice.deploy_configuration(
    cpu_cores=cpu_cores,
    memory_gb=memory_gb,
    auth_enabled=auth_enabled
)

service = Model.deploy(
    workspace=ws,
    name=service_name,
    models=[model],
    inference_config=inference_config,
    deployment_config=deploy_config,
    deployment_target=None,
)

# Wait for the deployment to complete
service.wait_for_deployment(show_output=True)
# Get the service endpoint
print(service.scoring_uri)
