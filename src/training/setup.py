from setuptools import setup, find_packages

setup(
    name='iris-training',
    version='0.1.0',
    description='Iris training module',
    author='Salman Shaik',
    author_email='salmanshaikdev@gmail.com',
    packages=find_packages(),
    install_requires=[
        'mlflow',
        'matplotlib',
        'scikit-learn'
    ],
)
