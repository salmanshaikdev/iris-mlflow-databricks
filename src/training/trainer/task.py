import mlflow
import joblib
import mlflow.sklearn
from mlflow.tracking.client import MlflowClient
from mlflow.entities import ViewType

import pandas as pd
import matplotlib.pyplot as plt

from numpy import savetxt

from sklearn.datasets import load_iris
from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler, LabelEncoder
from sklearn.metrics import accuracy_score
from data_transformation import encode_class_labels, scale_features

def train_model():
    # Load the iris dataset
    db = load_iris()
    X = db.data
    y = db.target

    y = encode_class_labels(y)
    X = scale_features(X)

    # Split the dataset into training and testing sets
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=42)

    # Create a decision tree classifier
    clf = DecisionTreeClassifier()

    # Train the model on the training data
    clf.fit(X_train, y_train)

    # Make predictions on the testing data
    y_pred = clf.predict(X_test)

    # Evaluate the model's accuracy on the testing data
    accuracy = accuracy_score(y_test, y_pred)

    print("Accuracy: " , accuracy)

    return clf, accuracy

def predict(model, request):
    # Convert the request data into a numpy array
    data = np.array(request).reshape(1, -1)

    # Make a prediction using the model
    prediction = model.predict(data)
    # Return the prediction
    return prediction[0]

# if __name__ == "__main__":
#     # Set the mlflow tracking URI
#     # mlflow.set_tracking_uri("http://0.0.0.0:5000")
#     # Train the model
#     clf, accuracy = train_model()
