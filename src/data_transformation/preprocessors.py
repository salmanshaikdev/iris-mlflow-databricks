import mlflow.sklearn
from sklearn.preprocessing import StandardScaler, LabelEncoder

def encode_class_labels:
    le = LabelEncoder()
    return le.fit_transform(y)

def scale_features:
    scaler = StandardScaler()
    return scaler.fit_transform(X)
