from flask import Flask, request, jsonify
import joblib

app = Flask(__name__)

# Load the trained model
model = joblib.load('../training/trainer/model.pkl')

# Define the endpoint for the API
@app.route('/predict', methods=['POST'])
def predict():
    # Get the input data from the request
    input_data = request.json['input_data']

    # Make a prediction using the model
    prediction = model.predict([input_data])

    # Return the prediction as a JSON response
    return jsonify({'prediction': prediction.tolist()})

@app.route('/health', methods=['GET'])
def health_check():
    # Return a 200 OK status to indicate that the API is up and running
    return 'OK', 200

if __name__ == '__main__':
    app.run(debug=True, port=5000)
