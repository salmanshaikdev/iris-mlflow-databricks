from azure.devops.connection import Connection
from msrest.authentication import BasicAuthentication
from azure.devops.v6_0.pipelines.models import (
    Pipeline,
    PipelineFolder,
    PipelineCreateParameters,
    PipelineResourceTypes,
    PipelineConfiguration,
    PipelineVariable,
    VariableGroup,
    VariableGroupCreateOrUpdateParameters,
    TaskGroupDefinition,
    TaskGroupCreateOrUpdateParameters,
    TaskGroupCreateOrUpdateParametersBase,
    AgentSpecification,
    TaskGroupRevision,
    TaskGroup,
    TaskGroupUpdateParameter,
    TaskGroupUpdateParametersBase,
    YamlFileSource,
    YamlPipelineProcess,
    YamlPipelineProcessResources,
    YamlPipelineProcessStages,
    YamlPipelineProcessStage,
    YamlPipelineProcessStep,
    YamlPipelineProcessStepTask,
    YamlPipelineProcessStepTaskInputs
)
import os

# Fill in with your Azure DevOps organization URL and personal access token (PAT)
org_url = '<your-organization-url>'
pat = '<your-pat>'

# Fill in with the name and folder path of the pipeline you want to create
pipeline_name = 'iris-model-pipeline'
pipeline_folder_path = './pipeline'

# Connect to the Azure DevOps organization
credentials = BasicAuthentication('', pat)
connection = Connection(base_url=org_url, creds=credentials)

# Get a handle to the Pipelines API client
pipelines_client = connection.clients.get_pipelines_client()

# Create a PipelineFolder object for the folder path
folder_path_parts = pipeline_folder_path.split('/')
pipeline_folder = None
for part in folder_path_parts:
    if part:
        if pipeline_folder:
            pipeline_folder = PipelineFolder(name=part, parent=pipeline_folder)
        else:
            pipeline_folder = PipelineFolder(name=part)

# Define the YAML for the pipeline
yaml = f"""
resources:
  pipelines:
  - pipeline: mlflow_pipeline
    source: "{pipeline_folder_path}/{pipeline_name}"
    trigger:
      branches:
      - master

stages:
- stage: Build
  jobs:
  - job: BuildModel
    pool:
      vmImage: 'ubuntu-latest'
    steps:
    - script: |
        pip install pandas
        pip install scikit-learn
        python ./pipeline/runner.py
      displayName: 'Build Model'
    - task: PublishPipelineArtifact@1
      inputs:
        artifactName: 'model'
        publishLocation: 'pipeline'
        targetPath: '$(Pipeline.Workspace)/model.pkl'
      displayName: 'Publish Model Artifact'
"""

# Create a PipelineConfiguration object for the pipeline configuration
pipeline_configuration = PipelineConfiguration(
    folder=pipeline_folder,
    repository=None,
    configuration=None,
    type=PipelineResourceTypes.PIPELINE,
    version=None
)

# Create a Pipeline object for the pipeline definition
pipeline_definition = Pipeline(
    name=pipeline_name,
    folder=pipeline_folder,
    configuration=pipeline_configuration,
    process=YamlPipelineProcess(yaml=YamlFileSource(yaml)),
    resources=YamlPipelineProcessResources(pipelines=[{
        "pipeline": "mlflow_pipeline",
        "project": "iris-mlflow-databricks"
    }]) 
)

# Create a PipelineCreateParameters object for the pipeline creation
pipeline_create_parameters = PipelineCreateParameters(
    folder=pipeline_folder,
    pipeline=pipeline_definition
)

# Create the pipeline
created_pipeline = pipelines_client.create_pipeline(pipeline_create_parameters, project="iris-mlflow-databricks")
